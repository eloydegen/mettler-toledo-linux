#!/usr/bin/python3.9

# Copyright (c) Eloy Degen, 2021
# SPDX-License-Identifier: MIT

import serial
import os
import time

# /dev/ttyS0 is the serial UART that is on the front connector panel. /dev/ttyS1 is the loadcell
# 0x16 = SYN
# 0x06 = ACK

def main():
	with serial.Serial('/dev/ttyS1', 9600, parity=serial.PARITY_EVEN, stopbits=serial.STOPBITS_ONE, bytesize=serial.SEVENBITS, timeout=1) as ser:
		print(ser.baudrate)
		print(ser.port)
		print(ser.name)
		print(str(ser.is_open))
		print(ser.timeout)

#		ser.read(size=1)
#		ser.write(b'\x16')
#		ser.write(b'\x1B\x3F')
#		ser.read(size=1)
#		ser.write(b'\x06')
#		ser.read(size=1)
#		ser.write(b'\x49\x0D\x0A')
#		print(str(ser.read(size=1)))
#		print(str(ser.read(size=1)))
#		print(str(ser.read(size=1)))
#		print(str(ser.read(size=1)))
#		print(str(ser.read(size=1)))
#		print(str(ser.read(size=1)))


		# In portman, the Unicorn application always does a read that times out
		# I speculate this is done for flushing the buffer, but I'm not sure
		try:
			ser.timeout = 0
			ser.read(size=1)
			ser.timeout = 1
		except:
			pass
#		print("quit, bye")
#		quit()

		# send ack lol
#		ser.write(b'\x06')

		# Maybe one failed read is needed
		#ser.read(size=1)

		# Send SYN
		ser.write(b'\x16')

		# Send address of loadcell, ESC and value 3F
		# Probably different value for other models
		ser.write(b'\x1B\x3F')

		#print("try to read ack...")
		#buf = ser.read(size=1)
		print("ack..?: " ) #+ str(buf))

		time.sleep(1)
		# Ack the ack :)
		ser.write(b'\x06')

		# Write, uhh, command?
		ser.write(b'\x49\x0D\x0A')

		# try reading ack
		#ser.read(size=1)

		# send ack
		ser.write(b'\x06')

		while True:
			# Command: S X I CR LF (SXI = Transmit data record immediately)
			ser.write(b'\x53\x58\x49\x0D\x0A')

			# ack
			ser.write(b'\x06')
			buf = ser.read(size=1)
			print(str(buf))
		#while True:
		#	buf = ser.read(size=1)
		#	print(str(buf))
		#	ser.write(b'\x06')

		quit()
		num = ser.write(b'\x16\x1B\x3F')
		print(str(num))
		data = ser.read(size=2)
		print("ser read: " + str(data))
		num = ser.write(b'\x49\x0D\x0A')
		print(str(num))
		while True:
			ser.write(b'\x06')
			print("try to read 1 char...")
			line = ser.read(size=1)
			ser.write(b'\x06')
			print(line)
			sleep(1)

# This program implements the CL interface for loadcells by Mettler Toledo
# Specification: https://web.archive.org/web/20211030134210/http://www.dragon.lv/exafs/equipment/704018_2_12.pdf
#
# The protocol has the following parameters:
# - Async transmission
# - 1 start bit
# - 7 data bits
# - 1 parity bit
# - 1 stop bit
#
# Returned byte structure:
# 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
# --- - ----------------------- -- ----------- -- --
# ID  S        DATA             S     UNIT     CR LF
#
# ID is the type of communication. Possible values:
# - "S": Result has been queried by S command, or the loadcell is in continuous mode
# - SPACE: Weight has not changed
# - "D": Weight is unstable
#
# S = NULL
#
# DATA is the main measurement value. Possible values:
#
#
# UNIT is the unit of the DATA that is returned. Possible values:
# -
# -
#
# CR is 0x0D
# LF is 0x0A

# dmesg output, wrong baudrate
# [   16.676437] 00:06: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A
# [   16.681045] 00:07: ttyS1 at I/O 0x2f8 (irq = 3, base_baud = 115200) is a 16550A

if __name__ == '__main__':
	main()

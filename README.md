# Linux on the Mettler Toledo UC3-GTT-A
Note: this repository is not in any way affiliated with or endorsed by Mettler Toledo International Inc.
## Operating system and CPU
This scale is based on Windows XP Embedded, running on an AMD Geode LX, which identifies as an i586 (what Intel would call "Pentium") but is actually an i686 with just one missing opcode: NOPL (No-OPeration Long)

Matteo Croce wrote a kernel patch back in 2009 to emulate this opcode in the Linux kernel, so a distribution that was compiled for i686 could be used. Back in the day, this was more of a performance optimization, but since a lot of distros have dropped support for the i586 these days, this patch is actually quite useful for hardware enablement. I haven't been able to apply this patch on a modern kernel yet.

In 2021, the same concept was [developed by Marcos Del Sol Vives](https://lkml.org/lkml/2021/6/26/132) while also adding CMOV support. This is not needed for the Geode LX, but nice for other systems which miss support for it.

The latest Debian kinda works, as long as you don't install packages that require this instruction. 

## Graphics 
IceWM works fine, XFCE 4 has a lot of applications that are going to crash. You have to boot with the following boot parameter to prevent having a garbled framebuffer:

`iomem=relaxed lxfb.mode_option=800x600@60`

For me, the keyboard did not work inside GRUB, so I added these boot parameters to `/etc/grub/default` and ran `update-grub2`

To get the X.org server to start, the following command has to be run: `sudo modprobe msr`

## Loadcell
The ZIF connector is labeled "KU60 Loadcell"

* https://www.onsemi.com/pdf/datasheet/mc74vhc32-d.pdf
* http://www.bothhandusa.com/datasheets/filter-transformer/pcmcia/16ST0009P-LF-RevA4-071113.pdf

Using portmon it was possible to intercept the traffic to the UART (COM port) and replicate it in HyperTerminal for example.

* http://web.archive.org/web/20211030134210/http://www.dragon.lv/exafs/equipment/704018_2_12.pdf

The mainboard of the scale has a ZIF socket which goes to the daughterboard of the loadcell. This daughterboard contains various ICs, and another flex cable to the actual loadcell component.

It connects to the mainboard over serial UART, which is `/dev/ttyS1` in Linux. `/dev/ttyS0` is in use for the serial UART D-Sub connector on the front of the machine, which is likely used for diagnostics in regular usage. 

### Parameters
* device file: `/dev/ttyS1` and `COM2` in Windows
* baudrate: 9600
* 1 stop bit
* Even parity bit

## Touchscreen
The touchscreen is an Hampshire Company TSHARC Analog Resistive. It needs to be calibrated with `xinput-calibrator`.

## Label printer

## Ethernet
An Ethernet firmware blob is needed. For Debian, the required file `d102e_ucode.bin` is packaged in `firmware-misc-nonfree` in the `e100` directory.

# Windows tricks
* Minimize the Unicorn app by opening task manager using `ctrl` + `shift` + `esc` and right-click on the task to minimize

# Ideas
Gentoo?
```
CHOST="i486-pc-linux-gnu"
COMMON_FLAGS="-Os -pipe -march=geode -mmmx -m3dnow -fomit-frame-pointer"
```
The problem with this approach is that any application using inline assembly containing the unsupported opcodes will still trigger invalid opcode signals (or generate undefined behaviour when the signal isn't even triggered, as happens with the x87 on the Vortex), even when compiled with these flags. So the kernel patch is still the preferred option.

* Qt/GTK app once the loadcell can be read from Linux

# References and useful links
* http://web.archive.org/web/20211031092836/http://strohmayers.com/linux/news/GeodeLX_as_686.pdf
* https://gitlab.alpinelinux.org/alpine/tsc/-/issues/20
* https://lists.debian.org/debian-user/2019/04/msg01093.html
* https://gitlab.freedesktop.org/xorg/driver/xf86-video-geode
* https://serverfault.com/questions/888285/up-to-date-distros-supporting-32-bit-i586-non-pae-specifically-for-alix-boards
* http://linux.voyage.hk/
* http://q-funk.blogspot.com/2021/01/help-needed-clean-up-and-submit-kms.html
## Mettler Toledo docs
* https://www.mt.com/mt_ext_files/Editorial/Generic/0/BA_IND429_Editorial-Generic_1132569403864_files/22013801B.pdf (SICS interface commands, page 35-36), I think that <esc> can mean any escape sequence, including the escape character itself
* 
